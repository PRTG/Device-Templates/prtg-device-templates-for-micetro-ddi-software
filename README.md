# PRTG Device Templates for Micetro DDI by Men&Mice

This project contains sample sensor templates for [PRTG REST Custom Sensors](https://www.paessler.com/manuals/prtg/rest_custom_sensor). The sensors will retrieve information via the API of the Micetro DDI software produced by Men&Mice.

These sensors are provided as an example of what can be monitored and can be used as a guide to create additional sensors, to meet specific needs. The Micetro API is documented using Swagger, making it easy to identify useful endpoints for monitoring with PRTG.

Details of how to create and use the sensors can be found in THIS blog post on te Paessler website. 
